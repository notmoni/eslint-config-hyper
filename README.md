# eslint-config-hyper
![CI](https://github.com/NotMoni/eslint-config-hyper/workflows/CI/badge.svg)
[![GitHub version](https://badge.fury.io/gh/NotMoni%2Feslint-config-hyper.svg)](https://badge.fury.io/gh/NotMoni%2Feslint-config-hyper)

> 🔥 An ESLint config to be hyped about 🚀

## Usage

We export one ESLint configurations for your usage.

### eslint-config-hyper

Our default export contains all of our ESLint rules, including ECMAScript 6+. It requires `eslint` and `eslint-plugin-import`.

1. Install the correct versions of each package, which are listed by the command:

  ```sh
  npm install --save-dev eslint eslint-config-hyper
  ```

2. Add `"extends": "hyper"` to your .eslintrc.


## Contributing
[**Propose or contribute a new rule ➡**](CONTRIBUTING.md)

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## Maintainers

- [Moni](https://github.com/NotMoni)

## License
[MIT](LICENSE)
