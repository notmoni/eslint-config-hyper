module.exports = {
  extends: [
    './rules/errors.js',
    './rules/es6.js',
    './rules/recommended.js',
    './rules/rules.js',
    './rules/imports.js',
    './rules/strict.js',
    './rules/node.js',
    './rules/stylistic.js',
    './rules/variables.js'
  ].map(require.resolve),
  env: {
    browser: true,
    node: true,
    amd: false,
    mocha: false,
    jasmine: false,
    es2020: true,
    commonjs: true
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  rules: {},
};
